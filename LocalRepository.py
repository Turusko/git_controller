import os


class LocalRepository():
    def __init__(self, name, location):
        self.name = name
        self.location = f"{location}\\{name}"

    def fetch(self):
        os.chdir(self.location)
        os.system("git fetch")

    def pull(self):
        os.chdir(self.location)
        os.system("git pull")
    
    def rebuild_solution(self):
        os.chdir(self.location)
        os.system("msbuild.exe /t:Clean;Build")
    
    def perform_npm_refresh(self):
        os.chdir(self.location)
        os.system("npm install")
        os.system("npm run build")
