from helpers import get_all_repositories


def job(repository):
    repository.fetch()
    repository.pull()
    if repository.location.__contains__("Zupa.Apps.Market"):
        repository.perform_npm_refresh()
    else:
        repository.rebuild_solution()


def main(source):
    for repository in get_all_repositories(source):
        print(f"Checking {repository.name}")
        job(repository)


def menu():
    location = input("Give Location of resource: ")
    print(f"location: {location}")
    if input("Update all repos? (y/n)") == "y":
        main(location)
        

if __name__ == '__main__':
    menu()

