import os
from LocalRepository import LocalRepository


def get_all_repositories(source):
    repository_list = list()
    for dir in os.listdir(source):
        if dir.__contains__("Zupa"):
            repository_list.append(LocalRepository(dir, source))
    return repository_list